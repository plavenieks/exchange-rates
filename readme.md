## Launch

1. Clone repo, cd in
2. Install dependencies `npm i`
3. Start server `npm run start`
4. Create bundle `npm run prod`

### Tests

`npm run test`
