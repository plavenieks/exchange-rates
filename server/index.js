const path = require("path");
const express = require("express");
const middlewareList = require("./middleware.js");
const app = express();
const VIEWS_PATH = path.normalize(
  path.join(__dirname, "/..", "/resources/views")
);

// Add middleware
middlewareList.forEach(middleware => {
  app.use(middleware.fn(middleware.settings));
});

// Template engine
app.set("views", VIEWS_PATH);
app.set("view engine", "jsx");
app.engine("jsx", require("express-react-views").createEngine());
app.use(express.static("./public"));

app.get("*", (req, res) => {
  res.status(200).render("index");
});

const server = require("http").Server(app);
server.listen(3000, () => {
  console.log("Started on port 3000");
});
