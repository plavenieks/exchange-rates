const helmet = require("helmet");
const bodyParser = require("body-parser");

module.exports = [
  {
    fn: helmet,
    settings: {
      referrerPolicy: true
    }
  },
  {
    fn: bodyParser.json,
    settings: {}
  },
  {
    fn: bodyParser.urlencoded,
    settings: {
      extended: false
    }
  }
];
