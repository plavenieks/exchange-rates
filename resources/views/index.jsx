const React = require("react");

class Layout extends React.Component {
  render() {
    return (
      <html>
        <head>
          <link rel="stylesheet" href="/style.css" />
          <title>Exchange rates</title>
        </head>
        <body>
          <div id="app" />
          <script src="/vendor.js" />
          <script src="/app.js" />
        </body>
      </html>
    );
  }
}

module.exports = Layout;
