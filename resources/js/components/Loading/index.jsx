import React from "react";
import PropTypes from "prop-types";

class Loading extends React.Component {
  render() {
    const { loading } = this.props;
    return (
      loading && (
        <div className="loading">
          <div className="loading-spinner">
            <div />
            <div />
            <div />
            <div />
          </div>
          Loading...
        </div>
      )
    );
  }
}

Loading.propTypes = {
  loading: PropTypes.bool.isRequired
};

export default Loading;
