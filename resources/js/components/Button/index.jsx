import React from "react";
import PropTypes from "prop-types";

class Button extends React.PureComponent {
  render() {
    const { color = "gray", onClick = () => {}, children } = this.props;
    return (
      <button className={`btn btn-${color}`} onClick={onClick}>
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  color: PropTypes.string,
  onClick: PropTypes.func
};

export default Button;
