import React from "react";
import PropTypes from "prop-types";

class TableHead extends React.Component {
  render() {
    const { uniqueKey, cols } = this.props;
    return (
      <thead key={`table-head-${uniqueKey}`}>
        <tr>
          {cols.map((col, index) => (
            <th key={`table-head-col-${uniqueKey}-${index}`}>{col.title}</th>
          ))}
        </tr>
      </thead>
    );
  }
}

TableHead.propTypes = {
  cols: PropTypes.array.isRequired,
  uniqueKey: PropTypes.string.isRequired
};

export default TableHead;
