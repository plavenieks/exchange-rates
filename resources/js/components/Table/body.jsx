import React from "react";
import PropTypes from "prop-types";

class TableBody extends React.Component {
  render() {
    const { uniqueKey, cols, data } = this.props;
    return (
      <tbody key={`table-body-${uniqueKey}`}>
        {data.map((row, rowIndex) => (
          <tr key={`table-body-${uniqueKey}-row-${rowIndex}`}>
            {cols.map((col, colIndex) => (
              <td
                key={`table-body-${uniqueKey}-row-${rowIndex}-col-${colIndex}`}
              >
                {row[col.key]}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  }
}

TableBody.propTypes = {
  cols: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  uniqueKey: PropTypes.string.isRequired
};

export default TableBody;
