import React from "react";
import PropTypes from "prop-types";
import TableHead from "./head.jsx";
import TableBody from "./body.jsx";

class Table extends React.PureComponent {
  constructor(props) {
    super(props);
    this.uniqueKey = new Date().getTime();
  }
  render() {
    const { cols, data, title } = this.props;
    return [
      !!title && <div key={`table-header-${this.uniqueKey}`}>{title}</div>,
      <table key={`table-${this.uniqueKey}`} className="table" cellSpacing="0">
        <TableHead uniqueKey={this.uniqueKey} cols={cols} />
        <TableBody uniqueKey={this.uniqueKey} cols={cols} data={data} />
      </table>
    ];
  }
}

Table.propTypes = {
  cols: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  title: PropTypes.string
};

export default Table;
