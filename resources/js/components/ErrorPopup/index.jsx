import React from "react";
import PropTypes from "prop-types";

class ErrorPopup extends React.PureComponent {
  render() {
    const { show, children } = this.props;
    return (
      show && [
        <div className="mask" key="mask" />,
        <div className="errorPopup" key="error">
          <div className="errorPopup-title">Error</div>
          <div className="errorPopup-close" />
          {children}
        </div>
      ]
    );
  }
}

ErrorPopup.propTypes = {
  show: PropTypes.bool.isRequired
};

export default ErrorPopup;
