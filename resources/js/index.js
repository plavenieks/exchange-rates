import "babel-polyfill";
import "../scss/main.scss";
import React from "react";
import ReactDOM from "react-dom";
import App from "./views/index.jsx";

ReactDOM.render(<App key="app" />, document.getElementById("app"));
