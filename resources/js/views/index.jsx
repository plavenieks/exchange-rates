import React from "react";
import { Provider } from "react-redux";
import store from "../store";
import Dashboard from "./Dashboard/index.jsx";

class App extends React.Component {
  render() {
    return (
      <Provider store={store} key="app">
        <Dashboard />
      </Provider>
    );
  }
}

export default App;
