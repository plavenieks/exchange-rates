import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { appClearError } from "../../store/app/actions";
import { ratesUpdateData } from "../../store/rates/actions";
import Loading from "../../components/Loading/index.jsx";
import ErrorPopup from "../../components/ErrorPopup/index.jsx";
import Table from "../../components/Table/index.jsx";
import Button from "../../components/Button/index.jsx";

const COLS = [
  {
    title: "Currency",
    key: "currency"
  },
  {
    title: "Rate",
    key: "rate"
  }
];

class DashBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.propsToState(props);
    this.handleClearError = this.handleFetchData.bind(this);
    this.handleFetchData = this.handleFetchData.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const state = this.propsToState(nextProps);
    this.setState(state);
  }
  propsToState(props) {
    let data = [];
    if (props && props.rates && props.rates.data) {
      Object.entries(props.rates.data).forEach(([currency, rate]) => {
        data.push({
          currency,
          rate
        });
      });
    }
    return {
      data
    };
  }
  handleClearError() {
    this.props.appClearError && this.props.appClearError();
  }
  handleFetchData() {
    this.props.ratesUpdateData && this.props.ratesUpdateData();
  }
  render() {
    const { app, rates } = this.props;
    const { loading, errorShow, errorMessage } = app;
    const { base, changedAt } = rates;
    const { data } = this.state;
    const hasData = data && !!data.length;
    return (
      <div key="view-dashboard" className="app">
        <h1 key="page-title">Exchange rates</h1>
        <div className="page" key="page-content">
          <Loading loading={loading} />
          <ErrorPopup show={errorShow}>{errorMessage}</ErrorPopup>
          {hasData && [
            <Table
              cols={COLS}
              data={data}
              title={`Rate base: ${base}`}
              key="data-table"
            />,
            <p key="data-updated-at">
              Data updated at: {changedAt.toDateString()}
            </p>
          ]}
          <Button color="blue" onClick={this.handleFetchData}>
            {hasData ? "Update rates" : "Get rates"}
          </Button>
        </div>
      </div>
    );
  }
}

DashBoard.propTypes = {
  app: PropTypes.object.isRequired,
  rates: PropTypes.object.isRequired
};

export default connect(
  state => state,
  {
    appClearError,
    ratesUpdateData
  }
)(DashBoard);
