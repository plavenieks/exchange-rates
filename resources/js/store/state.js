export default {
  app: {
    loading: false,
    errorShow: false,
    errorMessage: ""
  },
  rates: {
    data: {},
    base: "EUR",
    changedAt: new Date()
  }
};
