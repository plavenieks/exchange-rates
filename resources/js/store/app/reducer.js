import initialState from "../state";
import * as types from "./types";

export default function reducer(state = initialState.app, action) {
  const newState = {};
  switch (action.type) {
    case types.APP_CHANGE_LOADING: {
      newState.loading = action.data;
      break;
    }
    case types.APP_SET_ERROR: {
      newState.errorShow = true;
      newState.errorMessage = data;
      break;
    }
    case types.APP_CLEAR_ERROR: {
      newState.errorShow = false;
      newState.errorMessage = "";
      break;
    }
    default:
      return state;
  }
  return {
    ...state,
    ...newState
  };
}
