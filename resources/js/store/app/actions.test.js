import * as actions from "./actions";
import * as types from "./types";

describe("app actions", () => {
  it("should set app loading to given value", () => {
    const value = true;
    const expectedAction = {
      type: types.APP_CHANGE_LOADING,
      data: value
    };
    expect(actions.appChangeLoading(value)).toEqual(expectedAction);
  });
  it("should set app error to given text", () => {
    const text = "Failed to connect to server";
    const expectedAction = {
      type: types.APP_SET_ERROR,
      data: text
    };
    expect(actions.appSetError(text)).toEqual(expectedAction);
  });
  it("should clear app error", () => {
    const expectedAction = {
      type: types.APP_CLEAR_ERROR
    };
    expect(actions.appClearError()).toEqual(expectedAction);
  });
});
