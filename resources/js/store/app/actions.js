import * as types from "./types";

export const appChangeLoading = data => ({
  type: types.APP_CHANGE_LOADING,
  data
});
export const appSetError = data => ({
  type: types.APP_SET_ERROR,
  data
});
export const appClearError = () => ({
  type: types.APP_CLEAR_ERROR
});
