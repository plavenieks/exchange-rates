import { combineReducers } from "redux";
import app from "./app/reducer";
import rates from "./rates/reducer";
export default combineReducers({
  app,
  rates
});
