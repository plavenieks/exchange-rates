import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import * as actions from "./actions";
import * as appTypes from "../app/types";
import * as types from "./types";
import initialState from "../state";
import { EXCHANGE_API } from "../../helpers/endpoints";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const API_RESPONSES = {
  success: {
    date: "2018-11-06",
    rates: {
      GBP: 0.87313,
      USD: 1.1428
    },
    base: "EUR"
  },
  invalid: {
    date: "2018-11-06",
    data: {
      GBP: 0.87313,
      USD: 1.1428
    },
    base: "EUR"
  }
};

describe("rate actions", () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it("calling ratesUpdateData: should set loading on, get data, switch loading off", () => {
    const base = initialState.rates.base;
    fetchMock.getOnce(`${EXCHANGE_API}&base=${base}`, {
      body: API_RESPONSES.success,
      headers: { "content-type": "application/json" }
    });

    const expectedActions = [
      { type: appTypes.APP_CHANGE_LOADING, data: true },
      { type: types.RATES_UPDATE_DATA, data: API_RESPONSES.success.rates },
      { type: appTypes.APP_CHANGE_LOADING, data: false }
    ];

    const store = mockStore(initialState);
    return store.dispatch(actions.ratesUpdateData()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("if invalid data structure, error message should be set", () => {
    const base = initialState.rates.base;
    fetchMock.getOnce(`${EXCHANGE_API}&base=${base}`, {
      body: API_RESPONSES.invalid,
      headers: { "content-type": "application/json" }
    });

    const expectedActions = [
      { type: appTypes.APP_CHANGE_LOADING, data: true },
      { type: types.RATES_CLEAR_DATA },
      { type: appTypes.APP_SET_ERROR, data: "Invalid data format." },
      { type: appTypes.APP_CHANGE_LOADING, data: false }
    ];

    const store = mockStore(initialState);
    return store.dispatch(actions.ratesUpdateData()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("if failed to fetch, error message should be set", () => {
    const base = initialState.rates.base;
    fetchMock.mock(`${EXCHANGE_API}&base=${base}`, 404);

    const expectedActions = [
      { type: appTypes.APP_CHANGE_LOADING, data: true },
      { type: appTypes.APP_SET_ERROR, data: "Request has failed. Try again!" },
      { type: appTypes.APP_CHANGE_LOADING, data: false }
    ];

    const store = mockStore(initialState);
    return store.dispatch(actions.ratesUpdateData()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
