import initialState from "../state";
import * as types from "./types";

export default function reducer(state = initialState.rates, action) {
  const newState = {};
  switch (action.type) {
    case types.RATES_UPDATE_DATA: {
      newState.changedAt = new Date();
      newState.data = action.data;
      break;
    }
    case types.RATES_CLEAR_DATA: {
      newState.changedAt = new Date();
      newState.data = {};
      break;
    }
    default:
      return state;
  }
  return {
    ...state,
    ...newState
  };
}
