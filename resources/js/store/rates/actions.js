import * as types from "./types";
import { EXCHANGE_API } from "../../helpers/endpoints";
import { appChangeLoading, appSetError } from "../app/actions";

export const ratesClearData = () => ({
  type: types.RATES_CLEAR_DATA
});
export const ratesUpdateData = () => async (dispatch, getState) => {
  let error = null;
  const base = getState().rates.base;
  dispatch(appChangeLoading(true));
  let response = await fetch(`${EXCHANGE_API}&base=${base}`)
    .then(async res => await res.json())
    .catch(e => {
      console.warn(e.message);
      error = "Request has failed. Try again!";
    });
  if (error) {
    dispatch(appSetError(error));
  } else {
    if (response && response.rates) {
      dispatch({
        type: types.RATES_UPDATE_DATA,
        data: response.rates
      });
    } else {
      dispatch(ratesClearData());
      error = "Invalid data format.";
      dispatch(appSetError(error));
    }
  }
  dispatch(appChangeLoading(false));
};
